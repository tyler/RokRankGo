module zone.yusei/rok

go 1.16

require (
	github.com/spf13/viper v1.10.1
	github.com/tidwall/gjson v1.13.0
)
