package rok

import (
	"github.com/spf13/viper"
	"github.com/tidwall/gjson"
	"log"
	"math/rand"
	"os"
	"sync"
	"time"
	rok "zone.yusei/rok/src/api"
)

type rank struct {
	isApplying bool
	ranks      [3]string
	index      int //保活头衔 0->公爵 1->建筑师 2->科学家
	trueIndex  int //当前真正申请的头衔下标
	x, y       int
	flag       bool    // 强制申请标志位
	change     [3]bool // 判断头衔保持期间，刷新队列标志头衔是否被人顶掉过 0->公爵 1->建筑师 2->科学家
	setTime    string
	expectTime string
	isInQueue  bool   // 头衔申请排队标志
	num        [3]int //各头衔申请人数
	used       int    // 0->申请中 1->已获得 2->超时取消  3->国王取消 4->自己取消
}

var Rank *rank

func init() {
	Rank = new(rank)
	Rank.ranks = [3]string{"duke", "architect", "scientist"}
	//Rank.watchConfigFile()
	Rank.readConfigUpdateXY()
}

func (r *rank) readConfigUpdateXY() {
	log.Println("read config file")
	workDir, _ := os.Getwd()
	viper.AddConfigPath(workDir)
	viper.SetConfigName("config")
	viper.SetConfigType("json")
	if err := viper.ReadInConfig(); err != nil {
		log.Panicln(err)
	}
	r.index = viper.GetInt("rank")
	r.flag = true
	r.x = int(int64(viper.GetInt("x")))
	r.y = int(int64(viper.GetInt("y")))

	log.Println(r.index, r.x, r.y)
}

//func (r *rank) watchConfigFile() {
//
//	workDir, _ := os.Getwd()
//	viper.AddConfigPath(workDir)
//	viper.SetConfigName("config")
//	viper.SetConfigType("json")
//	if err := viper.ReadInConfig(); err != nil {
//		log.Panicln(err)
//	}
//
//	r.readConfigUpdateXY()
//
//	viper.WatchConfig()
//	viper.OnConfigChange(func(e fsnotify.Event) {
//		log.Println(e.Name, " config file changed.")
//		if err := viper.ReadInConfig(); err != nil {
//			log.Panicln(err)
//		}
//		r.readConfigUpdateXY()
//	})
//
//}

func (r *rank) update(js string) {
	flag := gjson.Get(js, "is_in_queue")
	if flag.Exists() {
		r.isInQueue = flag.Bool()

		r.used = int(gjson.Get(js, "queue.used").Int())
		r.setTime = gjson.Get(js, "queue.set_time").String()
		r.expectTime = gjson.Get(js, "queue.expect_time").String()

		x, y := int(gjson.Get(js, "queue.intx").Int()), int(gjson.Get(js, "queue.inty").Int())
		if x != r.x || y != r.y {
			r.x = x
			r.y = y
			viper.Set("x", r.x)
			viper.Set("y", r.y)
			viper.Set("rank", gjson.Get(js, "queue.rank").String())
			viper.WriteConfig()
		}

		r.trueIndex = int(gjson.Get(js, "queue.rank").Int())
		r.num[0] = int(gjson.Get(js, "info.rank0.num").Int())
		r.num[1] = int(gjson.Get(js, "info.rank1.num").Int())
		r.num[2] = int(gjson.Get(js, "info.rank2.num").Int())
	}
}

func (r *rank) refreshRankList() error {
	log.Println("refresh rank list...")
	result, err := rok.RefreshRankList()
	if err != nil {
		log.Println("refresh rank list", r.ranks[r.index], "failed, err=", err)
		return err
	}

	log.Println("response result:", result)

	r.update(result)

	return nil
}

func (r *rank) applyRank() error {
	log.Println("apply for", r.ranks[r.index], "...")
	result, err := rok.ApplyRank(r.index, r.x, r.y)
	if err != nil {
		log.Println("apply for", r.ranks[r.index], "failed, err=", err)
		return err
	}

	log.Println("response result:", result)

	r.update(result)

	return nil
}

func (r *rank) Start() {
	var wg sync.WaitGroup
	wg.Add(1)
	go r.service()
	wg.Wait()
}

func (r *rank) service() {
	rand.Seed(time.Now().UnixNano())
	log.Println("rok rank assistant running...")
	for {
		r.refreshRankList()

		switch r.used {
		case 0:
			if r.isApplying {
				log.Println(r.ranks[r.index], "rank is waiting in line getting time about at", r.expectTime)
			} else {
				log.Println("found super is applying for", r.ranks[r.trueIndex], "getting time about at", r.expectTime)
				log.Println("keeping rank in sync with super from", r.ranks[r.index], "to", r.ranks[r.trueIndex])
				r.index = r.trueIndex
				r.isApplying = true
			}
		case 1:
			if r.index == r.trueIndex {
				if r.isApplying {
					log.Println("you have got", r.ranks[r.index], "rank at", r.setTime)
					log.Println(r.ranks[r.index], "rank status is watching...")
				}
			} else {
				log.Println("found super has got", r.ranks[r.trueIndex], "at", r.setTime)
				log.Println("keeping rank in sync with super from", r.ranks[r.index], "to", r.ranks[r.trueIndex])
				r.index = r.trueIndex
			}
			r.isApplying = false
		case 2, 3, 4:
			if r.index == r.trueIndex {
				log.Println("you apply for", r.ranks[r.index], "canceled by yourself, timeout or king")
			} else {
				log.Println("you apply for", r.ranks[r.index], ", but true", r.ranks[r.trueIndex]+" rank canceled by yourself, timeout or king")
			}
			r.flag = true
		}

		if r.used != 0 {
			if r.flag || !r.isApplying && r.num[r.index] != 0 {
				r.change[r.index] = true
			}

			for _, v := range r.num {
				if v != 0 {
					log.Println(r.index, "rank queue:", r.ranks, r.num)
					break
				}
			}
		}

		if r.change[r.index] {
			// not in applying queue, reapply rank
			r.applyRank()
			r.isApplying = true
			r.flag = false
			r.change[r.index] = false
		}

		time.Sleep(time.Duration(rand.Intn(10)+10) * time.Second)
	}
}
