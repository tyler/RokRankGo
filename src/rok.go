package main

import (
	"io"
	"log"
	"os"
	rok "zone.yusei/rok/src/service"
)

func main() {
	workDir, _ := os.Getwd()
	logFile, err := os.OpenFile(workDir+"/rok.log", os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	if err != nil {
		panic(err)
	}
	defer logFile.Close()
	log.SetFlags(log.Lshortfile | log.Ldate | log.Lmicroseconds)
	log.SetOutput(io.MultiWriter(os.Stdout, logFile))

	rok.Rank.Start()
}
